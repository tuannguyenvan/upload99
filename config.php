<?php
// HTTP
define('HTTP_SERVER', 'http://upload99.local/');

// HTTPS
define('HTTPS_SERVER', 'http://upload99.local/');

// DIR
define('DIR_APPLICATION', 'D:/WorkSpace/htdocs/upload99/catalog/');
define('DIR_SYSTEM', 'D:/WorkSpace/htdocs/upload99/system/');
define('DIR_LANGUAGE', 'D:/WorkSpace/htdocs/upload99/catalog/language/');
define('DIR_TEMPLATE', 'D:/WorkSpace/htdocs/upload99/catalog/view/theme/');
define('DIR_CONFIG', 'D:/WorkSpace/htdocs/upload99/system/config/');
define('DIR_IMAGE', 'D:/WorkSpace/htdocs/upload99/image/');
define('DIR_CACHE', 'D:/WorkSpace/htdocs/upload99/system/storage/cache/');
define('DIR_DOWNLOAD', 'D:/WorkSpace/htdocs/upload99/system/storage/download/');
define('DIR_LOGS', 'D:/WorkSpace/htdocs/upload99/system/storage/logs/');
define('DIR_MODIFICATION', 'D:/WorkSpace/htdocs/upload99/system/storage/modification/');
define('DIR_UPLOAD', 'D:/WorkSpace/htdocs/upload99/system/storage/upload/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_DATABASE', 'upload99');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');
